-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-03-2022 a las 12:44:34
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bpichincha`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` char(36) COLLATE utf8_spanish_ci NOT NULL,
  `contrasenia` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `persona_id` char(36) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `contrasenia`, `estado`, `persona_id`) VALUES
('2487265b-7b85-473e-a3f0-9326e1239a30', '1111455', 1, '1e77a541-492e-4a17-9de7-5e1a3cb52e90'),
('311d2f09-550a-4ec6-9db7-beddaf937e85', '777777777', 1, '6361a303-5c84-4245-a332-5739a5147c49'),
('c0ff1a48-856c-40d8-aff9-ce0fb107ed02', '123456', 1, 'f66b854b-39a8-4fc1-ae42-c956dda0e6cb');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE `cuenta` (
  `id` char(36) COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `numero_cuenta` int(20) NOT NULL,
  `saldo_inicial` decimal(12,2) NOT NULL,
  `saldo_actual` decimal(12,2) DEFAULT NULL,
  `tipo_cuenta` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `cliente_id` char(36) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cuenta`
--

INSERT INTO `cuenta` (`id`, `estado`, `numero_cuenta`, `saldo_inicial`, `saldo_actual`, `tipo_cuenta`, `cliente_id`) VALUES
('63fa1698-26f4-4fbe-9fdc-6b98a51361ac', 1, 434241, '2000.00', '1600.00', 'Ahorros', '2487265b-7b85-473e-a3f0-9326e1239a30'),
('ef127637-dac5-4127-b68f-fe3cdd9e18db', 1, 55622278, '1000.00', '1000.00', 'Ahorros', 'c0ff1a48-856c-40d8-aff9-ce0fb107ed02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimiento`
--

CREATE TABLE `movimiento` (
  `id` char(36) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `saldo` decimal(12,2) NOT NULL,
  `tipo_movimiento` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `valor` decimal(12,2) NOT NULL,
  `cuenta_id` char(36) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `movimiento`
--

INSERT INTO `movimiento` (`id`, `fecha`, `saldo`, `tipo_movimiento`, `valor`, `cuenta_id`) VALUES
('38ece26b-e586-4f93-8e3f-a693a766a47e', '2022-03-28', '2150.00', 'DEPOSITO', '150.00', '63fa1698-26f4-4fbe-9fdc-6b98a51361ac'),
('9adf8f1f-34e8-40ed-bf5f-e9550de04c07', '2022-03-28', '1650.00', 'RETIRO', '500.00', '63fa1698-26f4-4fbe-9fdc-6b98a51361ac'),
('ddfa188b-43e5-44b7-b61d-7f8219c740ec', '2022-03-28', '1600.00', 'RETIRO', '50.00', '63fa1698-26f4-4fbe-9fdc-6b98a51361ac');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id` char(36) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `edad` int(11) NOT NULL,
  `genero` char(25) COLLATE utf8_spanish_ci NOT NULL,
  `identificacion` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `direccion`, `edad`, `genero`, `identificacion`, `nombre`, `telefono`) VALUES
('1e77a541-492e-4a17-9de7-5e1a3cb52e90', 'Brisas del Mar Sector 4', 32, 'Femenino', '0705202802', 'Blanca Chuqui', '0991133994'),
('6361a303-5c84-4245-a332-5739a5147c49', 'La Carolina', 45, 'Masculino', '0981234789', 'Jose Lema', '0991133994'),
('f66b854b-39a8-4fc1-ae42-c956dda0e6cb', 'Palmeras y 14va Sur', 33, 'Masculino', '0704257518', 'Jose Alvear', '0990875184');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `IDX_cliente_persona_id` (`persona_id`);

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNI_cuenta_id` (`id`),
  ADD KEY `IDX_cuenta_cliente_id` (`cliente_id`);

--
-- Indices de la tabla `movimiento`
--
ALTER TABLE `movimiento`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNI_movimiento_id` (`id`),
  ADD KEY `IDX_movimiento_cuenta_id` (`cuenta_id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `FK_persona_cliente` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD CONSTRAINT `FK_cliente_cuenta` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `movimiento`
--
ALTER TABLE `movimiento`
  ADD CONSTRAINT `FK_cuenta_movimiento` FOREIGN KEY (`cuenta_id`) REFERENCES `cuenta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
