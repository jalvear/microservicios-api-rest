package com.prueba.banco.responde;

import java.util.Date;

public class EstadoCuentaResponse {
    
    private Date fecha;
    private String cliente;
    private Integer numeroCuenta;
    private String tipoCuenta;
    private double saldoInicial;
    private boolean estado;
    private String Movimiento;
    private double valorMovimiento;
    private double saldo;

    
    public EstadoCuentaResponse() {
    }

    public Date getFecha() {
        return fecha;
    }
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    public String getCliente() {
        return cliente;
    }
    public void setCliente(String cliente) {
        this.cliente = cliente;
    }
    public Integer getNumeroCuenta() {
        return numeroCuenta;
    }
    public void setNumeroCuenta(Integer numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }
    public String getTipoCuenta() {
        return tipoCuenta;
    }
    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }
    public double getSaldoInicial() {
        return saldoInicial;
    }
    public void setSaldoInicial(double saldoInicial) {
        this.saldoInicial = saldoInicial;
    }
    public boolean isEstado() {
        return estado;
    }
    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    public String getMovimiento() {
        return Movimiento;
    }
    public void setMovimiento(String movimiento) {
        Movimiento = movimiento;
    }
    public double getValorMovimiento() {
        return valorMovimiento;
    }
    public void setValorMovimiento(double valorMovimiento) {
        this.valorMovimiento = valorMovimiento;
    }
    public double getSaldo() {
        return saldo;
    }
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    
}
