package com.prueba.banco.responde;

import lombok.Data;

@Data
public class MovimientoResponse {
    
    private Integer numeroCuenta;
    private String tipoCuenta;
    private double saldo;
    private boolean estado;
    private String Movimiento;
    private double valor;
    private String mensaje;
    
    
    public MovimientoResponse() {
    }

    public MovimientoResponse(Integer numeroCuenta, String tipoCuenta, double saldoInicial, boolean estado,
            String movimiento, double valor, String mensaje) {
        this.numeroCuenta = numeroCuenta;
        this.tipoCuenta = tipoCuenta;
        this.saldo = saldoInicial;
        this.estado = estado;
        Movimiento = movimiento;
        this.valor = valor;
        this.mensaje = mensaje;
    }
    
    public Integer getNumeroCuenta() {
        return numeroCuenta;
    }
    public void setNumeroCuenta(Integer numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }
    public String getTipoCuenta() {
        return tipoCuenta;
    }
    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }
    public double getSaldo() {
        return saldo;
    }
    public void setSaldo(double saldoInicial) {
        this.saldo = saldoInicial;
    }
    public boolean isEstado() {
        return estado;
    }
    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    public String getMovimiento() {
        return Movimiento;
    }
    public void setMovimiento(String movimiento) {
        Movimiento = movimiento;
    }
    public double getValor() {
        return valor;
    }
    public void setValor(double valor) {
        this.valor = valor;
    }
    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    
}
