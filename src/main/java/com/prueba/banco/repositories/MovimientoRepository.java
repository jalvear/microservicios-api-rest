package com.prueba.banco.repositories;

import com.prueba.banco.entity.Movimientos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/***
 * @author: José Alvaer
 * @since: 27-03-2022
 * @apiNote: Crud de la entidad Movimiento
 */

@Repository
public interface MovimientoRepository extends CrudRepository<Movimientos, String>{
    
}
