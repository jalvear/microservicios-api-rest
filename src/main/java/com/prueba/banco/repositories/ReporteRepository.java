package com.prueba.banco.repositories;

import com.prueba.banco.entity.Movimientos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/***
 * @author: José Alvear
 * @since : 2022-03-19
 * Repository para obtenter el reporte del estado de cuenta
 */

@Repository
public interface ReporteRepository extends JpaRepository<Movimientos, String>{

    //@Query("SELECT m FROM movimiento m WHERE m.fecha BETWEEN ?1 AND ?2 ")
    //ArrayList<Movimientos> findByDate(String incio, String fin);
}
