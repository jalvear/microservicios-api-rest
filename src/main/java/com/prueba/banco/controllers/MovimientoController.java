package com.prueba.banco.controllers;


import java.util.ArrayList;
import java.util.Optional;

import com.prueba.banco.entity.Movimientos;
import com.prueba.banco.request.MovimientoRequest;
import com.prueba.banco.responde.MovimientoResponse;
import com.prueba.banco.services.MovimientoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/movimientos")
public class MovimientoController {
    
    @Autowired
    MovimientoService movimientoService;

    @GetMapping
    public ResponseEntity<ArrayList<Movimientos>> getMovimientos(){
        return new ResponseEntity< >(this.movimientoService.getAllMovimientos(), HttpStatus.OK);
    } 

    @GetMapping (path = "/getMovById/{id}")
	public ResponseEntity<Optional<Movimientos>> obtenterClientePorID (@PathVariable ("id") String id){
		return ResponseEntity.ok(movimientoService.buscarMovimientoPorId(id));
	}

    @PostMapping()
    public ResponseEntity<MovimientoResponse>  addCuenta(@RequestBody MovimientoRequest movimiento){
        return new ResponseEntity<>(this.movimientoService.addMovimiento(movimiento), HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/eliminar/{id}")
    public String eliminarMovimientoPorId(@PathVariable("id") String id){
        boolean ban = this.movimientoService.eliminarMovimiento(id);
        if(ban) return "Se elimininó el movimiento con el id: "+id;
        else return "No se pudo eliminar el movimiento con el id: "+id;
    }


}
