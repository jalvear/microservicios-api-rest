package com.prueba.banco.controllers;

import java.util.ArrayList;
import java.util.Optional;

import com.prueba.banco.entity.Cliente;
import com.prueba.banco.entity.Persona;
import com.prueba.banco.request.ClienteRequest;
import com.prueba.banco.services.ClienteService;
import com.prueba.banco.services.PersonaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/clientes")
public class ClienteController {
    
    @Autowired
    PersonaService personaService;

    @Autowired
    ClienteService clienteService;

    @GetMapping()
    public ResponseEntity<ArrayList<Persona>> getClientes(){
        return new ResponseEntity< >(this.personaService.obtenerPersonas(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Persona>  addCliente(@Validated @RequestBody ClienteRequest cliente){
        //Persona p = this.clienteService.addCliente(cliente);
        return new ResponseEntity<>(this.personaService.addCliente(cliente), HttpStatus.CREATED);
    }

    @PostMapping(path = "/actualizar/{id}")
    public ResponseEntity<Cliente>  actualizarCliente(@RequestBody ClienteRequest cliente, @PathVariable("id") String id){
        return new ResponseEntity<>(this.personaService.actualizarPersona(cliente, id), HttpStatus.OK);
    }


    @DeleteMapping(path = "/eliminar/{id}")
    public String eliminarClientePorId(@PathVariable("id") String id){
        boolean ban = this.personaService.eliminarPersona(id);
        if(ban) return "Se elimininó la persona con el id: "+id;
        else return "No se pudo eliminar la persona con el id: "+id;
    }

    @GetMapping (path = "/getPersonaById/{id}")
	public ResponseEntity<Optional<Persona>> obtenterClientePorID (@PathVariable ("id") String id){
		return ResponseEntity.ok(personaService.BuscarPorId(id));
	}
}
