package com.prueba.banco.controllers;

import java.util.ArrayList;
import java.util.Optional;
import com.prueba.banco.entity.Cuenta;
import com.prueba.banco.request.CuentaResquest;
import com.prueba.banco.services.CuentaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/cuentas")
public class CuentaController {
    
    @Autowired
    CuentaService cuentaService;

    @GetMapping
    public ResponseEntity<ArrayList<Cuenta>> getCuentas(){
        return new ResponseEntity< >(this.cuentaService.getCuentas(), HttpStatus.OK);
    } 

    @GetMapping (path = "/getCuentaById/{id}")
	public ResponseEntity<Optional<Cuenta>> obtenterClientePorID (@PathVariable ("id") String id){
		return ResponseEntity.ok(cuentaService.buscarPorId(id));
	}

    @PostMapping()
    public ResponseEntity<Cuenta>  addCuenta(@Validated @RequestBody CuentaResquest cuenta){
        return new ResponseEntity<>(this.cuentaService.addCuenta(cuenta), HttpStatus.CREATED);
    }

    @PostMapping("/actualizarCuenta")
    public ResponseEntity<Cuenta>  actualizarCuenta(@Validated @RequestBody Cuenta cuenta){
        return new ResponseEntity<>(this.cuentaService.actualizarCuenta(cuenta), HttpStatus.OK);
    }

    @DeleteMapping(path = "/eliminar/{id}")
    public String eliminarCuentaPorId(@PathVariable("id") String id){
        boolean ban = this.cuentaService.eliminarCuenta(id);
        if(ban) return "Se elimininó la cuenta con el id: "+id;
        else return "No se pudo eliminar la cuenta con el id: "+id;
    }

}
