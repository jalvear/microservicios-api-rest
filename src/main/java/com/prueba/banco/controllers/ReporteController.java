package com.prueba.banco.controllers;

import java.util.ArrayList;
import com.prueba.banco.entity.Movimientos;
import com.prueba.banco.services.ReporteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/reportes")
public class ReporteController {
    
    @Autowired
    ReporteService reporteService;

    @GetMapping (path = "/fechas/{fechaDesde}/{fechaHasta}")
	public ArrayList<Movimientos> estadoCuentaPorFechas (@PathVariable ("fechaDesde") String fechaDesde, @PathVariable ("fechaHasta") String fechaHasta){
		
        return (reporteService.findByDate(fechaDesde, fechaHasta));
	}
}
