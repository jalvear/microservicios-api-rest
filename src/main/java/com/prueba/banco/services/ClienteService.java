package com.prueba.banco.services;

import java.util.ArrayList;
import java.util.Optional;

import com.prueba.banco.entity.Cliente;

public interface ClienteService {
   
    public ArrayList<Cliente> getClientes();
    public Cliente guardarCliente(Cliente c);
    public Optional<Cliente> getClientePorId(String id);
}
