package com.prueba.banco.services;

import java.util.ArrayList;
import java.util.Optional;

import com.prueba.banco.entity.Movimientos;
import com.prueba.banco.request.MovimientoRequest;
import com.prueba.banco.responde.MovimientoResponse;

public interface MovimientoService {
    
    public ArrayList<Movimientos> getAllMovimientos();
    public ArrayList<Movimientos> getMovimientosPorCuenta(Integer numeroCuenta);
    public MovimientoResponse addMovimiento(MovimientoRequest movimientoRequest); 
    public Optional<Movimientos> buscarMovimientoPorId(String id);
    public boolean eliminarMovimiento(String id);

}
