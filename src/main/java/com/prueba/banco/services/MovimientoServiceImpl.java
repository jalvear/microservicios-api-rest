package com.prueba.banco.services;

import java.util.ArrayList;
import java.util.Optional;
import com.prueba.banco.entity.Cuenta;
import com.prueba.banco.entity.Movimientos;
import com.prueba.banco.repositories.CuentaRepository;
import com.prueba.banco.repositories.MovimientoRepository;
import com.prueba.banco.request.MovimientoRequest;
import com.prueba.banco.responde.MovimientoResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovimientoServiceImpl implements MovimientoService {

    @Autowired
    MovimientoRepository movimientoRepository;

    @Autowired
    CuentaRepository cuentaRepository;

    @Override
    public ArrayList<Movimientos> getAllMovimientos() {
        
        return (ArrayList<Movimientos>)movimientoRepository.findAll();
    }

    @Override
    public ArrayList<Movimientos> getMovimientosPorCuenta(Integer numeroCuenta) {
        return null;
    }

    @Override
    public MovimientoResponse addMovimiento(MovimientoRequest movimientoRequest) {
        
        MovimientoResponse response = new MovimientoResponse();
        try {
            Optional<Cuenta> cuenta = cuentaRepository.findById(movimientoRequest.getCuentaID());
            Movimientos movimiento = new Movimientos();
            System.out.println("Cuenta");
            System.out.println(cuenta.get());

            if(cuenta.get().getId() != null){
                if(cuenta.get().isSaldoCuenta() || cuenta.get().isSaldoNoDisponible(movimientoRequest.getValor(), movimientoRequest.getTipoMovimiento())){
                    response.setMensaje("Saldo no Disponible");
                    return response;
                }else{
                    
                    movimiento.setFecha(movimientoRequest.getFecha());
                    movimiento.setTipoMovimiento(movimientoRequest.getTipoMovimiento().toUpperCase());
                    movimiento.setValor(movimientoRequest.getValor());
                    movimiento.setSaldo(movimientoRequest.getTipoMovimiento().toUpperCase().compareTo("DEPOSITO") == 0 ? 
                    cuenta.get().getSaldoActual() + movimientoRequest.getValor() : cuenta.get().getSaldoActual() - movimientoRequest.getValor());

                    movimiento.setCuenta(cuenta.get());
                    Movimientos mov = movimientoRepository.save(movimiento);
                    
                    //Se actualiza el valor actual de la cuenta
                    cuenta.get().setSaldoActual(mov.getSaldo());

                    //Se realiza la actualización en la Base de Datos
                    cuentaRepository.save(cuenta.get());
                    
                    response.setEstado(true);
                    response.setMovimiento(mov.getTipoMovimiento());
                    response.setNumeroCuenta(mov.getCuenta().getNumeroCuenta());
                    response.setTipoCuenta(mov.getCuenta().getTipoCuenta());
                    response.setSaldo(mov.getSaldo());
                    response.setValor(mov.getValor());
                    response.setMensaje("Transacción realizada con éxito!");
                }
            }else{
                response.setMensaje("Cuenta no existe. Imposible realizar la transacción");
            }
            
            
            return response;
        } catch (Exception e) {
            response.setMensaje("Transacción no Realizada!");
            return response;
        }
    }

    @Override
    public Optional<Movimientos> buscarMovimientoPorId(String id) {
        return movimientoRepository.findById(id);
    }

    @Override
    public boolean eliminarMovimiento(String id) {
        try {
            movimientoRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
}
