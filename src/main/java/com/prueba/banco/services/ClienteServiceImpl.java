package com.prueba.banco.services;

import java.util.ArrayList;
import java.util.Optional;

import com.prueba.banco.entity.Cliente;
import com.prueba.banco.repositories.ClienteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    @Override
    public ArrayList<Cliente> getClientes() {
        return (ArrayList<Cliente>) clienteRepository.findAll();
    }

    @Override
    public Cliente guardarCliente(Cliente c) {
        return clienteRepository.save(c);
    }

    @Override
    public Optional<Cliente> getClientePorId(String id) {
        return clienteRepository.findById(id);
    }
    
}
