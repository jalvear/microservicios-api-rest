package com.prueba.banco.services;

import java.util.ArrayList;
import java.util.Optional;

import com.prueba.banco.entity.Cliente;
import com.prueba.banco.entity.Persona;
import com.prueba.banco.repositories.ClienteRepository;
import com.prueba.banco.repositories.PersonaRepository;
import com.prueba.banco.request.ClienteRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonaService {
    
    @Autowired
    PersonaRepository personaRepository;

    @Autowired
    ClienteRepository clienteRepository;
    
    public ArrayList<Persona> obtenerPersonas(){
        return (ArrayList<Persona>) personaRepository.findAll();
    }

    public Persona addCliente(ClienteRequest cliente){

        Persona p = new Persona();
        //ArrayList<Cliente> listaClientes = new ArrayList<Cliente> ();
        p.setDireccion(cliente.getDireccion());
        p.setEdad(cliente.getEdad());
        p.setGenero(cliente.getGenero());
        p.setIdentificacion(cliente.getIdentificacion());
        p.setNombre(cliente.getNombre());
        p.setTelefono(cliente.getTelefono());
        p = personaRepository.save(p);

        Cliente c = new Cliente();
        c.setContrasenia(cliente.getContrasenia());
        c.setEstado(cliente.isEstado());
        c.setPersona(p);

        clienteRepository.save(c);

        //listaClientes.add(c);
        //p.setClientes(listaClientes);

        return p;
    }

    public Cliente actualizarPersona(ClienteRequest cliente, String id){
        Persona p = new Persona();
        Optional<Persona> pers = personaRepository.findById(id);
        p = pers.get();
        p.setDireccion(cliente.getDireccion());
        p.setEdad(cliente.getEdad());
        p.setGenero(cliente.getGenero());
        p.setIdentificacion(cliente.getIdentificacion());
        p.setNombre(cliente.getNombre());
        p.setTelefono(cliente.getTelefono());
        p = personaRepository.save(p);

        Cliente c = new Cliente();
        c.setContrasenia(cliente.getContrasenia());
        c.setEstado(cliente.isEstado());
        c.setPersona(p);
        //clienteRepository.save(c);

        return c;
    }

    public boolean eliminarPersona(String id){
        try{
            personaRepository.deleteById(id);
            return true;
        }catch(Exception err){
            return false;
        }
    }

    public Optional<Persona> BuscarPorId (String id) {
		return personaRepository.findById(id);
	}
}
