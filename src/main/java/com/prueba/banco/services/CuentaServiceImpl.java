package com.prueba.banco.services;

import java.util.ArrayList;
import java.util.Optional;

import com.prueba.banco.entity.Cliente;
import com.prueba.banco.entity.Cuenta;
import com.prueba.banco.repositories.ClienteRepository;
import com.prueba.banco.repositories.CuentaRepository;
import com.prueba.banco.request.CuentaResquest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CuentaServiceImpl implements CuentaService {

    @Autowired
    CuentaRepository cuentaRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Override
    public ArrayList<Cuenta> getCuentas() {
        return (ArrayList<Cuenta>)cuentaRepository.findAll();
    }

    @Override
    public Cuenta addCuenta(CuentaResquest cuentaRequest) {
        try {
            Cuenta cuenta = new Cuenta();
            cuenta.setNumeroCuenta(cuentaRequest.getNumeroCuenta());
            cuenta.setTipoCuenta(cuentaRequest.getTipoCuenta());
            cuenta.setSaldoInicial(cuentaRequest.getSaldoInicial());
            cuenta.setEstado(cuentaRequest.isEstado());
            cuenta.setSaldoActual(cuentaRequest.getSaldoInicial());

            Optional<Cliente> c = clienteRepository.findById(cuentaRequest.getClienteID());
            if(c != null){
                cuenta.setCliente(c.get());
            }
            
            return cuentaRepository.save(cuenta);
        } catch (Exception e) {
            return null;
        }
        
    }

    @Override
    public boolean eliminarCuenta(String id) {
        try {
            cuentaRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Cuenta actualizarCuenta(Cuenta c) {
        return cuentaRepository.save(c);
    }

    @Override
    public Optional<Cuenta> buscarPorId(String id) {
        return cuentaRepository.findById(id);
    }
}
