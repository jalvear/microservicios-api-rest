package com.prueba.banco.services;

import java.util.ArrayList;
import com.prueba.banco.entity.Movimientos;

public interface ReporteService {
    ArrayList<Movimientos> findByDate(String incio, String fin);
}
