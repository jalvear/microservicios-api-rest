package com.prueba.banco.services;

import java.util.ArrayList;
import java.util.Optional;
import com.prueba.banco.entity.Cuenta;
import com.prueba.banco.request.CuentaResquest;

public interface CuentaService {
    
    public ArrayList<Cuenta> getCuentas();
    public Cuenta addCuenta(CuentaResquest c);
    public boolean eliminarCuenta(String id);
    public Cuenta actualizarCuenta(Cuenta c);
    public Optional<Cuenta> buscarPorId(String id);
}
