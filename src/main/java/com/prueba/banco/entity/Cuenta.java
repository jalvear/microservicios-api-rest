package com.prueba.banco.entity;

import java.beans.Transient;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "cuenta")
public class Cuenta {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(unique = true, nullable = false, length = 36)
    private String id;

    @Column(name = "numero_cuenta", nullable = false, length = 20)
    private Integer numeroCuenta;

    @Column(name = "tipo_cuenta", nullable = false, length = 100)
    private String tipoCuenta;

    @Column(name = "saldo_inicial", nullable = false)
    private double saldoInicial;

    @Column(name = "saldo_actual", nullable = false)
    private double saldoActual;

    @Column(name = "estado", columnDefinition = "tinyint(1) default 1")
    private boolean estado;

    @JoinColumn(name = "cliente_id", referencedColumnName = "id")
    @ManyToOne
    private Cliente cliente;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(Integer numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public double getSaldoInicial() {
        return saldoInicial;
    }

    public void setSaldoInicial(double saldoInicial) {        
        this.saldoInicial = saldoInicial;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    

    public double getSaldoActual() {
        return saldoActual;
    }

    public void setSaldoActual(double saldoActual) {
        this.saldoActual = saldoActual;
    }

    @Transient
    public boolean isSaldoCuenta(){
        return this.saldoActual == 0 ? true : false;
    }

    public boolean isSaldoNoDisponible(Double valorRetiro, String tipoMovimiento){
        return ((tipoMovimiento.toUpperCase().compareTo("RETIRO") == 0) && (this.saldoActual < valorRetiro)) ? true : false;
    }
}
