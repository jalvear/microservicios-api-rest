package com.prueba.banco.request;

import java.util.Date;

import lombok.NonNull;

public class MovimientoRequest {
   
    @NonNull
    private Date fecha;

    @NonNull
    private String tipoMovimiento;

    @NonNull
    private Double valor;

    @NonNull
    private String cuentaID;

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getCuentaID() {
        return cuentaID;
    }

    public void setCuentaID(String cuentaID) {
        this.cuentaID = cuentaID;
    }

    

}
